const mongoose = require("mongoose");

const checkoutSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "First Name is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required : [true, "Product ID is required"]
			},
			quantity: {
				type: Number,
				required : [true, "Quantity is required"]
			}
		}
	],
	checkoutAmount: {
		type: Number,
		required : [true, "Total Amount is required"]
	},
	checkoutOn: {
		type: Date,
		default: Date.now,
		required : [true, "Purchase Date is required"]
	}
})

module.exports = mongoose.model("Checkout", checkoutSchema);
