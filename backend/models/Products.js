const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName : {
		type : String,
		required : [true, "Course is required."]
	},
	productDescription : {
		type : String,
		required : [true, "Description is required."]
	},
	productPrice: {
		type : Number,
		required : [true, "Price is required."]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	image: {
		type: String, 
		required: true
	},
	createdOn : {
		type : Date,
		default : Date.now,
	},
	userOrders : [{
		userId : {
			type : mongoose.Schema.Types.ObjectId,
			required : [true, "User ID is required."],
			ref: 'User'
		},
		checkoutAmount : {
			type : Number,
			required : [true, "Total Amount is required."],
			ref: 'User'
		},
		checkoutOn: {
			type: Date,
		default: new Date()
		}
	}]
});

module.exports = mongoose.model("Products", productSchema);
