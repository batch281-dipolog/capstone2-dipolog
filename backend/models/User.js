const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName : {
      type : String,
      required : [true, "Firstname is required."]
    },
    lastName : {
      type : String,
      required : [true, "Lastname is required."]
    },
    email : {
      type : String,
      required : [true, "Email is required."]
    },
    password : {
      type : String,
      required : [true, "Password is required."]
    },
    isAdmin : {
      type : Boolean,
      default : false
    },
    orderedProduct: [
      {
        products: [
          {
            productId: {
              type: mongoose.Schema.Types.ObjectId,
              ref: "Product",
              required: true,
            },
            quantity: {
              type: Number,
              required: true,
            },
            price: {
              type: Number,
              ref: "Product",
            },
          },
        ],
        totalAmount: {
          type: Number,
          required: [true, "Total Amount is required."],
        },
        purchasedOn: {
          type: Date,
          default: new Date(),
        },
      },
    ],
  }
);

module.exports = mongoose.model("User", userSchema);
