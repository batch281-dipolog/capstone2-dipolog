const User = require("../models/User");
const Products = require("../models/Products")
const express = require ("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../middleware/auth");

// (1) Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// (2) Route for Admin Registration
router.post("/register-admin", (req, res) => {
  userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController));
});

// (3) User registration route
router.post('/register', async (req, res) => {
  try {
    const resultFromController = await userController.registerUser(req.body);
    res.status(resultFromController.success ? 200 : 400).json(resultFromController);
  } catch (error) {
    res.status(500).json({ success: false, message: 'An internal server error occurred.' });
  }
});


// (4) Route for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// (5) Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
  
  const userData = auth.decode(req.headers.authorization);
  userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// (6) Route for creating an Order
router.post("/add-to-cart", auth.verify, async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    const productId = req.body.productId;
    const quantity = req.body.quantity;

    // Retrieve product information from the database
    const product = await Products.findById(productId);

    if (!product) {
      return res.status(404).json({ message: "Product not found" });
    }

    const price = product.productPrice; // Get the price from the retrieved product

    const data = {
      userData,
      productId,
      quantity,
      price
    };

    userController.addToCart(data).then(resultFromController =>
      res.send(resultFromController)
    );
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
});

// (7) Route for retrieving ordered products for the authenticated user
router.get("/myOrders", auth.verify, userController.getUserOrders);

// (8) Route Total Price
router.get("/totalPrice", auth.verify, userController.getTotalPrice);

// (9) Route for deleting an ordered product by ObjectId
router.delete("/delete/:objectId", auth.verify, async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    const objectId = req.params.objectId;
    const userId = userData.id;

    userController.deleteOrderedProduct(userId, objectId).then((resultFromController) => {
      if (resultFromController) {
        res.json({ message: "Ordered product deleted successfully" });
      } else {
        res.status(404).json({ message: "Ordered product not found" });
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
});


// (10) Route for updating the quantity of an ordered product by ObjectId
router.patch("/update/:objectId", auth.verify, async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    const objectId = req.params.objectId;
    const userId = userData.id;
    const quantity = req.body.quantity;

    userController.updateOrderedProductQuantity(userId, objectId, quantity).then((resultFromController) => {
      if (resultFromController) {
        res.json({ message: "Ordered product quantity updated successfully" });
      } else {
        res.status(404).json({ message: "Ordered product not found" });
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
});


// (11) Set User isAdmin (Admin Only)
router.patch("/:userId/set-admin", auth.verify, (req, res) => {
  const decodedToken = auth.decode(req.headers.authorization);
  
  if (decodedToken.isAdmin === true) {
    userController.setAsAdmin(req.params.userId, true)
      .then(resultFromController => {
        if (resultFromController) {
          res.send({ success: true, message: "User set as admin successfully" });
        } else {
          res.send({ success: false, message: "Failed to set user as admin" });
        }
      })
      .catch(error => {
        res.send({ success: false, message: error.message || "Failed to set user as admin" });
      });
  } else {
    res.send({ success: false, message: "Unauthorized: Only admins can set users as admin" });
  }
});


// (12) Retrieve all orders (Admin Only)
router.get("/orders", auth.verify, (req, res) => {
  const decodedToken = auth.decode(req.headers.authorization);

  if (decodedToken.isAdmin === true) {
    userController.getAllOrders()
      .then(orders => {
        res.send({ success: true, data: orders });
      })
      .catch(error => {
        res.send({ success: false, message: error.message || "Failed to retrieve orders" });
      });
  } else {
    res.send({ success: false, message: "Unauthorized: Only admins can access orders" });
  }
});


// (13) Route for checking out an order
router.post("/checkout", auth.verify, async (req, res) => {
  try {
    const data = {
      userData: auth.decode(req.headers.authorization),
      productId: req.body.productId,
      checkoutAmount: req.body.totalAmount,
    };

    const resultFromController = await userController.checkoutOrder(data);

    if (resultFromController) {
      res.send({ success: true, message: "Checkout successful", data: data });
    } else {
      res.send({ success: false, message: "Checkout failed", data: data });
    }
  } catch (error) {
    console.error("Error while processing checkout:", error);
    res.status(500).send({ success: false, message: "Internal server error" });
  }
});

module.exports = router;
