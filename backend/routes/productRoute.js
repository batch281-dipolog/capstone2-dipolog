const express = require ("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../middleware/auth");

// Route for creating product
router.post("/createProduct", auth.verify, productController.createProduct);

// Route for retrieving all products
router.get("/all", productController.allProducts);

// Route for retrieving all active products
router.get("/active", productController.activeProducts);

// Route for retrieving single product
router.get("/:productId", productController.getProduct);

// Route for updating product information
router.put("/update/:productId", auth.verify, productController.updateProduct);

// Route for deleting a product
router.delete("/delete/:productId", auth.verify, productController.deleteProduct);

// Route for archiving a product
router.patch("/archives/:productId", auth.verify, productController.archiveProduct);

// Route for activating a product
router.patch("/activate/:productId", auth.verify, productController.activateProduct);

module.exports = router;

