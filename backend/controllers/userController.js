const User = require("../models/User");
const Products = require("../models/Products")
const bcrypt = require("bcrypt");
const auth = require("../middleware/auth");

// (1) User check email
module.exports.checkEmailExists = (reqBody) => {
  return User.find({email : reqBody.email}).then(result => {
    // The find method returns a record if a match is found
    if(result.length > 0){
      return true;

    // No duplicate emails found
    // The user is not registered in the database
    } else {
      return false;
    };
  });
};

// (2) User Registration-Admin
module.exports.registerUser = (reqBody) => {

  // Creates a variable named "newUser" and insantiates a new "User" object using the Mondoose model
  let newUser = new User({
    firstName : reqBody.firstName,
    lastName : reqBody.lastName,
    email : reqBody.email,
    password : bcrypt.hashSync(reqBody.password, 10)
  })

  return newUser.save().then((user, error) => {

    // User registration failed
    if(error){
      return false;

    // User registration successful
    } else {
      return true;
    }
  })

};


// (3) User Registration
// Client registration and email checking if available
module.exports.registerUser = async (reqBody) => {
  try {
    // Check if the email already exists in the database
    const existingUser = await User.findOne({ email: reqBody.email });
    if (existingUser) {
      return { success: false, message: 'Email already exists. Please use a different email.' };
    }

    // Create a new User instance with hashed password
    const newUser = new User({
      email: reqBody.email,
      firstName: reqBody.firstName,
      lastName: reqBody.lastName,
      password: bcrypt.hashSync(reqBody.password, 10),
    });

    // Save the new user to the database
    const savedUser = await newUser.save();
    return { success: true, message: 'User registered successfully.', user: savedUser };
  } catch (error) {
    return { success: false, message: 'An error occurred during user registration.', error: error.message };
  }
};


// (4) User Authentication Login
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){
			result = "Please Register First."
			return result

		// User exists
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// If the password matches
			if(isPasswordCorrect){
				// Generate an access token
				// Uses the createAccessToken method defined in the auth.js file
				return { access: auth.createAccessToken(result)}

			// Passwords do not match
			} else {
				result = "Please check your password or email."
				return result
			}
		}
	})
};


// (5) Retrieve user details
module.exports.getProfile = (data) => {

  return User.findById(data.userId).then(result => {

    result.password = "******";

    return result;

  });

};

// (6) AddtoCart an Order
module.exports.addToCart = async (data) => {
  let isProductAddedToCart = await User.findById(data.userData.id).then(user => {
    if (!user) {
      console.log("User not found");
      return false;
    }

    const totalAmount = data.price * data.quantity;

    user.orderedProduct.push({
      products: [
        { productId: data.productId, quantity: data.quantity, price: data.price }
      ],
      totalAmount: totalAmount,
      purchasedOn: new Date()
    });

    return user.save().then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  });

  return isProductAddedToCart;
};

// userController.js

// (7) Get all ordered products for a user
module.exports.getUserOrders = async (req, res) => {
  try {
    const { id: userId } = auth.decode(req.headers.authorization);

    const user = await User.findById(userId);
    if (user) {
      if (user.orderedProduct.length > 0) {
        const orders = user.orderedProduct.map((order) => {
          const products = order.products.map((product) => ({
            _id: product._id,
            productId: product.productId,
            quantity: product.quantity,
            price: product.price,
            totalAmount: product.quantity * product.price, // Calculate totalAmount
          }));

          const totalAmount = products.reduce((acc, curr) => acc + curr.totalAmount, 0); // Calculate totalAmount for the order

          return {
            _id: order._id,
            products,
            totalAmount,
            purchasedOn: order.purchasedOn,
          };
        });
        res.send(orders);
      } else {
        res.send(`User ${user.email} has no orders.`);
      }
    } else {
      res.status(404).send("User not found");
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
};


// (8) Total price for all items
module.exports.getTotalPrice = async (req, res) => {
  const { id: userId } = auth.decode(req.headers.authorization);
  try {
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).send({ message: "User not found" });
    }

    const totalAmount = user.orderedProduct.reduce((total, order) => total + order.totalAmount, 0);
    const totalPriceResponse = `The total price of your orders is ${totalAmount} pesos`;

    return res.send({ totalAmount, message: totalPriceResponse });
  } catch (error) {
    console.error("Error getting the total amount of your order:", error);
    return res.status(500).send("Error getting the total amount of your order");
  }
};


// (9) Delete an ordered product by ObjectId
module.exports.deleteOrderedProduct = async (userId, objectId) => {
  try {
    const user = await User.findById(userId);

    if (!user) {
      console.log("User not found");
      return false;
    }

    const orderedProductIndex = user.orderedProduct.findIndex((order) => order._id.equals(objectId));

    if (orderedProductIndex === -1) {
      console.log("Ordered product not found");
      return false;
    }

    user.orderedProduct.splice(orderedProductIndex, 1);

    await user.save();

    return true;
  } catch (error) {
    console.log("Error deleting ordered product:", error);
    return false;
  }
};


// (10) Update the quantity of an ordered product by ObjectId
module.exports.updateOrderedProductQuantity = async (userId, objectId, quantity) => {
  const user = await User.findById(userId);

  if (!user) {
    console.log("User not found");
    return false;
  }

  const orderedProduct = user.orderedProduct.id(objectId);

  if (!orderedProduct) {
    console.log("Ordered product not found");
    return false;
  }

  // Update the quantity of the ordered product
  orderedProduct.products[0].quantity = quantity;

  // Update the totalAmount based on the updated quantity
  orderedProduct.totalAmount = orderedProduct.products[0].price * quantity;

  return user.save().then((savedUser) => {
    return true;
  }).catch((error) => {
    console.log("Error updating ordered product quantity:", error);
    return false;
  });
};

// (11) Set a User as admin
module.exports.setAsAdmin = (userId, isAdmin) => {
  let updateActiveField = {
    isAdmin: isAdmin
  };

  return User.findByIdAndUpdate(userId, updateActiveField)
    .then(user => {
      return user ? true : false; // Check if the user exists and was updated successfully
    })
    .catch(error => {
      throw new Error("Failed to set user as admin");
    });
};

// (12) Retrieve all orders
module.exports.getAllOrders = () => {
  return User.find({}) // You can add any necessary conditions to filter the users if needed
    .populate("orderedProduct") // Assuming "orderedProduct" is the field referencing the orders in the User schema
    .then(users => {
      // Extract the ordered products from the users
      const orders = users.reduce((acc, user) => {
        return acc.concat(user.orderedProduct);
      }, []);

      return orders;
    })
    .catch(error => {
      throw new Error("Failed to retrieve orders");
    });
};

// (13) Checkout
module.exports.checkoutOrder = async (data) => {

  let isProductUpdated = await Products.findById(data.productId).then(products => {
    // Adds the userId in the products userOrders array
    products.userOrders.push({
      userId : data.userData,
      checkoutAmount: data.checkoutAmount
    });

    return products.save().then((products, error) => {
      if(error){
        return false;
      } else {
        return true;
      };
    });

  });

  if(isProductUpdated){
    return true;
  // User enrollment failure
  } else {
    return false
  };
};


