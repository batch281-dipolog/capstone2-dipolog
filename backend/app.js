const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoute");
const productRoutes = require("./routes/productRoute");

const app = express();

// Connect to our MOngoDB
mongoose.connect("mongodb+srv://wilsdipolog:B2N0J1jDr9GDhPCM@wdc028-course-booking.sspbnpg.mongodb.net/e-commerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error."))

db.once('open', ()=> console.log('Now connected to MongoDB Alas!'));

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "userRoutes" file
app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env. PORT || 4000}`)
})